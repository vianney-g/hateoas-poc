module Orders exposing (main)

import Browser
import Decimal exposing (Decimal)
import Html
import Html.Attributes as Attr
import Html.Events as Event
import Http
import Json.Decode as J
import Url


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , subscriptions = subscriptions
        , update = update
        , view = view
        }


init : () -> ( Model, Cmd Msg )
init _ =
    let
        model_ : Model
        model_ =
            AskingOrder
    in
    ( model_, Cmd.none )



-- MODEL


type alias Order =
    { mktpId : String
    , mktp : String
    , linesPage : OrderLinesPage
    , actions : List Action
    }


type alias OrderLinesPage =
    { lines : List OrderLine
    , pagination : Pagination
    }


type alias OrderLine =
    { id : String
    , price : Price
    }


type alias Price =
    { amount : Decimal
    , currency : String
    }


type alias Action =
    { label : String, url : Url.Url }


type alias Pagination =
    { next : Maybe Url.Url
    , previous : Maybe Url.Url
    }


type Model
    = AskingOrder
    | FetchingOrder Int
    | DisplayingOrder Order
    | DisplayingError Error


type Error
    = HttpError Http.Error
    | DomainError String



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- MESSAGES


type Msg
    = FetchOrder Int
    | GotOrder (Result Http.Error Order)
    | DisplayOrder Order
    | DisplayError Error


update : Msg -> Model -> ( Model, Cmd Msg )
update msg _ =
    case msg of
        FetchOrder orderId ->
            ( FetchingOrder orderId, fetchOrder orderId )

        DisplayOrder order ->
            ( DisplayingOrder order, Cmd.none )

        GotOrder (Ok order) ->
            ( DisplayingOrder order, Cmd.none )

        GotOrder (Err err) ->
            ( DisplayingError (HttpError err), Cmd.none )

        DisplayError err ->
            ( DisplayingError err, Cmd.none )



-- API requests stuff


urlForOrderId : Int -> String
urlForOrderId orderId =
    "http://localhost:8080/order/" ++ String.fromInt orderId


fetchOrder : Int -> Cmd Msg
fetchOrder orderId =
    let
        url =
            urlForOrderId orderId
    in
    Http.get
        { url = url
        , expect = Http.expectJson GotOrder orderDecoder
        }



-- API result parser


orderDecoder : J.Decoder Order
orderDecoder =
    let
        pagination =
            { next = Nothing, previous = Nothing }

        linesPage =
            { lines = [], pagination = pagination }

        order =
            { mktpId = "AZE"
            , mktp = "Amazon"
            , linesPage = linesPage
            , actions = []
            }
    in
    J.succeed order



-- VIEWS


view : Model -> Html.Html Msg
view model =
    case model of
        AskingOrder ->
            askOrderView

        FetchingOrder _ ->
            fetchingOrderView

        DisplayingOrder order ->
            displayOrderView order

        DisplayingError err ->
            displayErrorView err


askOrderView : Html.Html Msg
askOrderView =
    Html.div []
        [ Html.input [ Attr.class "toto", Attr.min "0", Attr.max "2", Event.onInput castOrder ] []
        ]


castOrder : String -> Msg
castOrder orderStr =
    case String.toInt orderStr of
        Just orderNb ->
            FetchOrder orderNb

        Nothing ->
            DisplayError (DomainError "Order id are integers")


fetchingOrderView : Html.Html Msg
fetchingOrderView =
    Html.div [] [ Html.text "Fetching..." ]


displayOrderView : Order -> Html.Html Msg
displayOrderView order =
    Html.div [] [ Html.text order.mktpId ]


displayErrorView : Error -> Html.Html Msg
displayErrorView err =
    Html.div [] [ Html.text (err2Str err) ]


err2Str : Error -> String
err2Str err =
    case err of
        HttpError http_err ->
            httpErr2Str http_err

        DomainError domain_err ->
            domain_err


httpErr2Str : Http.Error -> String
httpErr2Str err =
    case err of
        Http.BadUrl url ->
            "Uh what's that? " ++ url

        Http.Timeout ->
            "Request is sleeping..."

        Http.NetworkError ->
            "Please find a network"

        Http.BadStatus status ->
            "Haha you get a response status " ++ String.fromInt status

        Http.BadBody body ->
            "Very strange body " ++ body
